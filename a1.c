
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>

#define PATHMAX 1000


void removeSubstr (char *string, const char *sub) {
    char *match;
    int len = strlen(sub);
    while ((match = strstr(string, sub))) {
        *match = '\0';
        strcat(string, match+len);
    }
    free(match);
}

int startsWithString( char* pathName, char*filterName){
    //printf("\n \n the chars, pathName, filterName are: %s %s \n \n",pathName, filterName);
    if (strncmp(pathName,filterName, strlen(filterName))==0)
        return 1;
    else
        return 0;
}

char* permis(char *pathName){

  //  printf("dir->d_name is: %s \n", pathName);

    struct stat fileStat;
    if(stat(pathName,&fileStat) < 0){
        printf("na");
        return strerror(errno);
    }
    char *myString = malloc(sizeof(char)*11+2);
    strcpy(myString,"");

    strcat(myString, (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
    strcat(myString, (fileStat.st_mode & S_IRUSR) ? "r" : "-");
    strcat(myString, (fileStat.st_mode & S_IWUSR) ? "w" : "-");
    strcat(myString, (fileStat.st_mode & S_IXUSR) ? "x" : "-");
    strcat(myString, (fileStat.st_mode & S_IRGRP) ? "r" : "-");
    strcat(myString, (fileStat.st_mode & S_IWGRP) ? "w" : "-");
    strcat(myString, (fileStat.st_mode & S_IXGRP) ? "x" : "-");
    strcat(myString, (fileStat.st_mode & S_IROTH) ? "r" : "-");
    strcat(myString, (fileStat.st_mode & S_IWOTH) ? "w" : "-");
    strcat(myString, (fileStat.st_mode & S_IXOTH) ? "x" : "-");

    return myString;

}


void recursiveListing(char* pathName, int beg){ //beginning

   // printf("Im here");
   char buffer[1024]=" ";

    DIR *d;
    struct dirent *dir;


    d = opendir(pathName);

    if (d==NULL)
        return;




    while ((dir = readdir(d)) != NULL) {
        if (dir->d_type == DT_DIR) {
            char path[1000];
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
                continue;


            snprintf(path, sizeof(path), "%s/%s", pathName, dir->d_name);


            sprintf(buffer,"%d",beg);
            printf("\n%s/%s",pathName, dir->d_name);  //* is used to pass the width specifier
            recursiveListing(path, beg + 2);
        } else {
           // printf("Im else here");
            sprintf(buffer,"%d",beg);
            printf("\n%s/%s", pathName, dir->d_name);

        }
    }
    free(dir);
    closedir(d);

}

void permissionRecursiveListing(char* path, size_t size,char *permName, char*original){ //beginning
    DIR *dir;
    struct dirent *entry;
    size_t len = strlen(path);

    if (!(dir=opendir(path))){
        printf("\nERROR\nCannot open path\n");
        return;
    }

   // printf("\n path is %s \n original is %s \n\n",path, original);

    //char *ret = strrchr(path,'/');
    //char *nextret = ret+1;
    if ((strcmp(permis(path),permName)==0) && (strcmp(path,original)!=0))
        puts(path);


    while((entry = readdir(dir)) != NULL){
        char *name = entry->d_name;
        if (entry->d_type == DT_DIR){
            if (  !strcmp(name, ".") || !strcmp(name, "..")   )
                continue;
            if (len + strlen(name) +2 > size){
            printf("path too long");
            }
            else{
                path[len]= '/';
                strcpy (path+len+1,name);
                permissionRecursiveListing(path,size,permName,original);
                path[len]='\0';
            }
        }
        else{
           // if (startsWithString(entry->d_name,filterName)==1)
             //   printf("%s/%s\n", path, name);

            char *temp = malloc(sizeof(char)*1000);
            strcpy(temp,path);
            strcat(temp,"/");
            strcat(temp,name);
            if ((strcmp(permis(temp),permName)==0) && (strcmp(temp,original)!=0))
                printf("%s/%s\n", path, name);
            free(temp);

        }
    }
    closedir(dir);
}

/*your program must function similar to the case when run with the “list recursive” options,
 though it must search only for SF ﬁles that have at least 2 section(s) with exactly 14 lines.*/

void filteredRecursiveListing(char* path, size_t size,char *filterName, char *original){ //beginning
    DIR *dir;
    struct dirent *entry;
    size_t len = strlen(path);

    if (!(dir=opendir(path))){
        printf("\nERROR\nCannot open path\n");
        return;
    }

    char *ret = strrchr(path,'/');
    char *nextret = ret+1;
    if( (startsWithString(nextret,filterName)==1)   && (strcmp(path,original)!=0) )
        puts(path);


    while((entry = readdir(dir)) != NULL){
        char *name = entry->d_name;
        if (entry->d_type == DT_DIR){
            if (  !strcmp(name, ".") || !strcmp(name, "..")   )
                continue;
            if (len + strlen(name) +2 > size){
            printf("path too long");
            }
            else{
                path[len]= '/';
                strcpy (path+len+1,name);
                filteredRecursiveListing(path,size,filterName,original);
                path[len]='\0';
            }
        }
        else{

            char *temp = malloc(sizeof(char)*1000);
            strcpy(temp,path);
            strcat(temp,"\n");
            strcat(temp,name);
            if( (startsWithString(entry->d_name,filterName)==1) && (strcmp(temp,original)!=0) ){

                 printf("%s/%s\n", path, name);
            }
            free(temp);

        }
    }
    closedir(dir);
}

void extract(char *pathName, int section_number, int line_number){


    int fd; //file descr
    fd = open(pathName,O_RDONLY);
    if (fd<0)
    {
        printf("ERROR\nInvalid file\n");
        return;
    }
    char magicField[2];
    lseek(fd,0,SEEK_SET); //SET the pointer to the beginning of the file
    read(fd, &magicField,1);
    //strncpy(&magicField[1],"\0",1);
    magicField[1]='\0';
    if (magicField[0]!='G')
    {
        printf("ERROR\nInvalid file\n");
        return ;

    }
     int16_t headerSize; //two bytes int
    read(fd,&headerSize,2);
    int32_t versionNr;
    read (fd,&versionNr,4);
    if((versionNr<21) || (versionNr>136))
        {
            printf("ERROR\nInvalid file\n");
            return ;
        }
    int8_t sectionNr;
    read(fd,&sectionNr,1);
    if ((sectionNr<2) || (sectionNr>17)){
       printf("ERROR\nInvalid file\n");
       return;
        }


    if( (section_number>sectionNr) || (section_number<0) )//not existing section
    {
        printf("ERROR\nInvalid section\n");
    }

    char sectionName[7];
    int8_t sectionType;
    int32_t sectionOffset;
    int32_t sectionSize;

    for (int i=0; i<section_number; ++i){ //go until the given section

        read(fd,&sectionName,6);
        read(fd,&sectionType,1);
        if(! ((sectionType==64)||(sectionType==89)||(sectionType==35)||(sectionType==13))  ){
           // printf("\n wrong sectiont TYPE \n");
            printf("ERROR\nInvalid file\n");
            return ;
        }

        read(fd, &sectionOffset,4);
        read(fd, &sectionSize,4);

    }

    char outp;
    int checkLine=1;

    printf("SUCCESS");
    if ((section_number==1) && (line_number==1))
        printf("%c",'\n');


    for (int i=0; i<sectionSize; ++i){

        lseek(fd,sectionOffset+sectionSize-i-1,SEEK_SET);
        read(fd,&outp,1);

        if (outp=='\n')
        {
          //  printf("\n new line detected \n \n");
            checkLine = checkLine +1; //counts in which line we currently are
        }
        if (checkLine>line_number){
           // printf("\nline number passed, exit time\n");
            i=sectionSize; //leave the loop
        }

        if (checkLine == line_number) //line found //print line content
        {

            printf("%c",outp);
        }

    }

    if (checkLine<line_number){
        printf("ERROR\nInvalid line\n");
      //  free(outLine);
        return;
    }

        return;




}

//we get the section nr of the file, we run extract_aux from 1 to section nr and it will return the final line
//nr, we count how much of them is equal with 14
//if wrong file, returns -1

int extract_aux (char *pathName, int section_number){ //returns 1 if the nr of line in the section is 14. otherwise -1

    int fd; //file descr
    fd = open(pathName,O_RDONLY);
    if (fd<0)
    {
      //  printf("ERROR\nInvalid file\n");
        return -1;
    }
    char magicField[2];
    lseek(fd,0,SEEK_SET); //SET the pointer to the beginning of the file
    read(fd, &magicField,1);
    strncpy(&magicField[1],"\0",1);
    if (magicField[0]!='G')
    {
      //  printf("ERROR\nInvalid file\n");
        return -1;

    }
     int16_t headerSize; //two bytes int
    read(fd,&headerSize,2);
    int32_t versionNr;
    read (fd,&versionNr,4);
    if((versionNr<21) || (versionNr>136))
        {
          //  printf("ERROR\nInvalid file\n");
            return -1;
        }
    int8_t sectionNr;
    read(fd,&sectionNr,1);
    if ((sectionNr<2) || (sectionNr>17)){
     //  printf("ERROR\nInvalid file\n");
       return -1;
        }


    if( (section_number>sectionNr) || (section_number<0) )//not existing section
    {
        printf("ERROR\nInvalid section\n");
        return -1;
    }

    char sectionName[7];
    int8_t sectionType;
    int32_t sectionOffset;
    int32_t sectionSize;

    for (int i=0; i<section_number; ++i){ //go until the given section

        read(fd,&sectionName,6);
        read(fd,&sectionType,1);
        if(! ((sectionType==64)||(sectionType==89)||(sectionType==35)||(sectionType==13))  ){
           // printf("\n wrong sectiont TYPE \n");
          //  printf("ERROR\nInvalid file\n");
            return -1;
        }

        read(fd, &sectionOffset,4);
        read(fd, &sectionSize,4);

    }

    char outp;
    int checkLine=1;

    //printf("SUCCESS");


    for (int i=0; i<sectionSize; ++i){

        lseek(fd,sectionOffset+sectionSize-i-1,SEEK_SET);
        read(fd,&outp,1);

        if (outp==0x0A)
        {
          //  printf("\n new line detected \n \n");
            checkLine = checkLine +1; //counts in which line we currently are
        }

    }

    //printf("\n \n number of line is in section %d is:   %d\n ",section_number, checkLine);

        if(checkLine == 14)
            return 1;
        else
            return -1;


}


int compatible(char *pathName){ //filters the file for findall

    //printf("IM HERE AT FINDALL %s\n \n ",pathName);

    //FIRST GET THE NUMBER OF SECTIONS
    int line_counter=0;

    int fd; //file descr
    fd = open(pathName,O_RDONLY);
    if (fd<0)
    {
       // printf("cannot open file");
        return -1;
    }


    //MAGIC

    char magicField[2];
    lseek(fd,0,SEEK_SET); //SET the pointer to the beginning of the file
    read(fd, &magicField,1);
    strncpy(&magicField[1],"\0",1);
    if (magicField[0]!='G')
    {
       // printf("ERROR\nwrong magic\n");
        return  -1;

    }

    //HEADER

    int16_t headerSize; //two bytes int
    read(fd,&headerSize,2);

    //VERSION

    int32_t versionNr;
    read (fd,&versionNr,4);
    if((versionNr<21) || (versionNr>136))
        {
          //  printf("ERROR\nwrong version\n");
            return -1;
        }


    //SECTIONS
    int8_t sectionNr;
    read(fd,&sectionNr,1);
    if ((sectionNr<2) || (sectionNr>17)){
      // printf("ERROR\nwrong sect_nr\n");
       return -1;
    }


    int fourteen;

    for (int ind = 1; ind <=sectionNr; ++ind){
        fourteen = extract_aux(pathName, ind);
        if (fourteen == 1)
            ++line_counter;
    }
    if(line_counter>=2)
        return 1;
    else
        return -1;

}

void findall (char *pathName, int beg){ //uses recursive listing

   char buffer[1024]=" ";

   char *temp = malloc(512*sizeof(char));
   char *comp = malloc(512*sizeof(char));

    DIR *d;
    struct dirent *dir;

    d = opendir(pathName);

    if (d==NULL){
        return;
        free(temp);
        free(comp);
    }


    printf("SUCCESS\n");

    while ((dir = readdir(d)) != NULL) {
        if (dir->d_type == DT_DIR) {
            char path[1000];
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
                continue;

            sprintf(buffer,"%d",beg);

            strcpy(temp,pathName);
            strcat(temp,"/");
            strcat(temp,dir->d_name);
             strcat(temp,"\n");

            strcpy(comp,pathName);
            strcat(comp,"/");
            strcat(comp,dir->d_name);
           // printf("\n%s/%s",pathName, dir->d_name);  //* is used to pass the width specifier
            int check = compatible(comp);
           if (check==1)
                printf("%s",temp);



            recursiveListing(path, beg + 2);
        } else {
           // printf("Im else here");
            sprintf(buffer,"%d",beg);

            strcpy(temp,pathName);
            strcat(temp,"/");
            strcat(temp,dir->d_name);
             strcat(temp,"\n");

            strcpy(comp,pathName);
            strcat(comp,"/");
            strcat(comp,dir->d_name);

            int check = compatible(comp);

            if (check==1)
                printf("%s",temp);

        }
    }
    free(temp);
    free(comp);
    closedir(d);

}


void checkSF (char *pathName){

    char *output = malloc(512*sizeof(char));
    strcpy(output,"SUCCESS\n");
   // printf("out: %s \n",output);


    int fd; //file descr
    fd = open(pathName,O_RDONLY);
    if (fd<0)
    {
        free(output);
        printf("cannot open file");
        return;
    }


    //MAGIC

    char magicField[2];
    lseek(fd,0,SEEK_SET); //SET the pointer to the beginning of the file
    read(fd, &magicField,1);
    strncpy(&magicField[1],"\0",1);
    if (magicField[0]!='G')
    {
        free(output);
        printf("ERROR\nwrong magic\n");
        return ;

    }

    //HEADER

    int16_t headerSize; //two bytes int
    read(fd,&headerSize,2);

    //VERSION

    int32_t versionNr;
    read (fd,&versionNr,4);
    if((versionNr<21) || (versionNr>136))
        {
            free(output);
            printf("ERROR\nwrong version\n");
            return ;
        }
    else{
      //  printf("  version matches\n");
        strcat(output,"version=");

        char *vtemp = malloc(4*sizeof(char));
        sprintf(vtemp,"%d",versionNr);
        strcat(output,vtemp);
        //printf("vers %d\n",versionNr); //EDDIG OK
        free(vtemp);

    }

    //SECTIONS
    int8_t sectionNr;
    read(fd,&sectionNr,1);
    if ((sectionNr<2) || (sectionNr>17)){
    free(output);
       printf("ERROR\nwrong sect_nr\n");
       return ;
    }
    else{
      //  printf("  sectionNr matches\n");
          strcat(output,"\nnr_sections=");

        char *snrtemp = malloc(3*sizeof(char));
        sprintf(snrtemp,"%d",sectionNr);
        strcat(output,snrtemp);
        //EDDIG OK
      //  printf("out= %s",output);
        free(snrtemp);
    }

    //section NAME
    char *temp = malloc(3*sizeof(char)); //to convert the indexes to char because they should be also printed
    char *typetemp = malloc(3*sizeof(char));
    char *sizetemp = malloc(6*sizeof(char));

    char sectionName[7];
    int8_t sectionType;
    int32_t sectionOffset;
    int32_t sectionSize;



    for (int i=1; i<=sectionNr; ++i){

        strcat(output,"\nsection"); //preparing output
        sprintf(temp,"%d",i);
        strcat(output,temp);
        strcat(output,": ");

        read(fd,&sectionName,6);
        strncpy(&sectionName[6],"\0",1);
        strcat(output,sectionName);
        strcat(output," ");

        read(fd,&sectionType,1);
        if(! ((sectionType==64)||(sectionType==89)||(sectionType==35)||(sectionType==13))  ){
           // printf("\n wrong sectiont TYPE \n");
            printf("ERROR\nwrong sect_types\n");
            free(output);
            free(sizetemp);
            free(typetemp);
            free(temp);
            return ;
        }
        sprintf(typetemp,"%d",sectionType);
        strcat(output,typetemp);
        strcat(output, " ");

        read(fd, &sectionOffset,4);

        read(fd, &sectionSize,4);
        sprintf(sizetemp,"%d",sectionSize);
        strcat(output,sizetemp);


    }

    free(temp);
    free(typetemp);
    free(sizetemp);

    printf("%s",output);


    free(output);
    return;
}

int main(int argc, char **argv){

    char *occursPath0;
    char *occursPath;
    char *occursPath2;
    char *occursPath3;
    char *occursFilterN0;
    char *occursFilterN;
    char *occursFilterN2;
    char *occursFilterN3;
    char *occursFilterP0;
    char *occursFilterP;
    char *occursFilterP2;
    char *occursFilterP3;

    char *occursSectionNr0;
    char *occursSectionNr;
    char *occursSectionNr2;
    char *occursSectionNr3;
    char *occursLineNr0;
    char *occursLineNr;
    char *occursLineNr2;
    char *occursLineNr3;


    const char path[6]="path=";
    const char filter[18]="name_starts_with=";
    const char permission[14]="permissions=";
    const char section[10]="section=";
    const char line[6]="line=";

    //FIRST PART

    if(argc == 2){
        if(strcmp(argv[1], "variant") == 0){ //if the parameter is "variant"
            printf("62773\n");
        }
        else{
            printf("ERROR \n");
            printf("Invalid directory path");
        }
    }
    // IF inputs are list and path=...

    else if (argc==3){
    //if string "path=" occurs in arguments
        occursPath = strstr(argv[2],path);
        occursPath0 = strstr(argv[1],path);

        if( ((strcmp(argv[1], "list")==0) && (occursPath !=NULL)) ||  ((strcmp(argv[2], "list")==0) && (occursPath0 !=NULL))   ){
           // printf("\n path maybe found \n"); //if only path should be listed

           char*pathName;

           if (occursPath!=NULL){

            pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
            strcpy(pathName, argv[2]);
            removeSubstr(pathName, path);

           }
           else{

            pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
            strcpy(pathName, argv[1]);
            removeSubstr(pathName, path);
           }
            //try to define that string as directory
            DIR *d;
            d = opendir(pathName);
            if (d == NULL)
            {
                //printf("\n ERROR \n Invalid directory path \n ");
                printf("ERROR \n");
                printf("Invalid directory path");
                return(1);
            }
            else
            {
                printf("SUCCESS");
                struct dirent *dir;
               // printf("\n directory is opened \n ");
                while((dir = readdir(d)) != NULL){

                    //check if it is a directory type and exclude "." and ".."

                        if(strcmp(dir->d_name,"..")!=0 && strcmp(dir->d_name,".")!=0) // if not . and .. then it is a directory name
                            printf("\n %s/%s  ", pathName, dir->d_name);
                }
            }
            closedir(d);
            free(pathName);

        }  // END OF list path combination

        //start parse path
        else if( ((strcmp(argv[1], "parse")==0)  && (occursPath!=NULL)) || ((strcmp(argv[2], "parse")==0)  && (occursPath0!=NULL)))
        {
            char *pathName;

            if(occursPath!=NULL){ //parse path=
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[2]);
                removeSubstr(pathName, path);
              //  if(checkSF(pathName)==-1)
            checkSF(pathName);


            }
            else{ //path= parse

                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[1]);
                removeSubstr(pathName, path);
                //if (checkSF(pathName)==-1)
                checkSF(pathName);


            }
            free(pathName);
        }

        //START findall path combination
        else if(( ((strcmp(argv[1], "findall")==0)  && (occursPath!=NULL)) || ((strcmp(argv[2], "findall")==0)  && (occursPath0!=NULL))))
        {
            char *pathName;
            if (occursPath!=NULL) //findall path
            {
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[2]);
                removeSubstr(pathName, path);
              //  if(checkSF(pathName)==-1)
                //findall(pathName);

                 findall(pathName,0);

            }
            else   //path findall
            {
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[1]);
                removeSubstr(pathName, path);
               //findall(pathName,0);

               //int a = compatible(pathName);
               //printf("a = %d",a);

              //  if(checkSF(pathName)==-1)
                findall(pathName,0);
            }
            free(pathName);
        }


        else{
       // printf("\n ERROR \n Invalid directory path \n");
        printf("ERROR \n");
        printf("Invalid directory path");

        }
    }
    // list + path + recursive COMBINATION

    else if(argc == 4)
    {
        //if the path string is the 2nd or 3rd parameter
        occursPath0 = strstr(argv[1],path);
        occursPath = strstr(argv[2],path);
        occursPath2 = strstr(argv[3],path);
        occursFilterN0 = strstr(argv[1],filter);
        occursFilterN = strstr(argv[2],filter);
        occursFilterN2 = strstr(argv[3],filter);
        occursFilterP0 = strstr(argv[1],permission);
        occursFilterP = strstr(argv[2],permission);
        occursFilterP2 = strstr(argv[3],permission);
        //if list path recursive OR list recursive path combination

        if( (  (strcmp(argv[1], "list")==0) && (occursPath !=NULL) && (strcmp(argv[3],"recursive")==0) )
         || (  (strcmp(argv[1], "list")==0) &&  (strcmp(argv[2],"recursive")==0) && (occursPath2!=NULL) )
         || (  (occursPath0 != NULL) &&  (strcmp(argv[2],"list")==0) && (strcmp(argv[3],"recursive")==0) )
         || (  (occursPath0 != NULL) &&  (strcmp(argv[2],"recursive")==0) && (strcmp(argv[3],"list")==0) )
         || (  (strcmp(argv[1], "recursive")==0) &&  (occursPath !=NULL) && (strcmp(argv[3],"list")==0) )
         || (  (strcmp(argv[1], "recursive")==0) &&  (strcmp(argv[2],"list")==0) && (occursPath2 != NULL) )


         ) {
            //printf("correct arc4 input");
            char *pathName;

            if(occursPath0!=NULL){

               pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[1]);
              //  removeSubstr(pathName, path);
                //printf("path is the second param");

            }
            else if (occursPath != NULL){

                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[2]);
               // removeSubstr(pathName, path);
                //printf("oath is the third param");

            }
            else {
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[3]);
               // removeSubstr(pathName, path);

            }



            removeSubstr(pathName, path);
            printf("SUCCESS");
            recursiveListing(pathName,0);

            free(pathName);

        }
        //check for path and filter

        else if( (  (strcmp(argv[1], "list")==0) && (occursPath !=NULL) && (occursFilterN2!=NULL) )
         || (  (strcmp(argv[1], "list")==0) &&  (occursPath2 !=NULL) && (occursFilterN!=NULL) )
         || (  (occursPath0 != NULL) &&  (strcmp(argv[2],"list")==0) && (occursFilterN2!=NULL) )
         || (  (occursPath0 != NULL) &&  (occursFilterN!=NULL) && (strcmp(argv[3],"list")==0))
         || (  (occursFilterN0 != NULL) &&  (strcmp(argv[2],"list")==0) && (occursPath2!=NULL) )
         || (  (occursFilterN0 != NULL) &&  (occursPath!=NULL) && (strcmp(argv[3],"list")==0))


          ) {

            char *pathName;
            char *filterName;


            if (occursPath0 != NULL){
                if (occursFilterN != NULL)
                {
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[1]);
                strcpy(filterName, argv[2]);
                }
                else //occursFilterN2!=NULL
                {
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[1]);
                strcpy(filterName, argv[3]);
                }
            }
            else if (occursPath != NULL){
                if (occursFilterN0 != NULL)
                {
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[2]);
                strcpy(filterName, argv[1]);
                }
                else //occursFilterN2!=NULL
                {
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[2]);
                strcpy(filterName, argv[3]);
                }
            }
            else { //path is the third argument
                if (occursFilterN0 != NULL)
                {
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[3]);
                strcpy(filterName, argv[1]);
                }
                else //occursFilterN!=NULL
                {
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                filterName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[3]);
                strcpy(filterName, argv[2]);
                }
            }
            removeSubstr(pathName,path);
                removeSubstr(filterName, filter);

            //filtered printing

            DIR *d;
            d = opendir(pathName);
            if (d == NULL)
            {
               // printf("\n ERROR \n Invalid directory path \n ");
                printf("ERROR \n");
                printf("Invalid directory path");
                return(1);
            }
            else
            {
                printf("SUCCESS");
                struct dirent *dir;
               // printf("\n directory is opened \n ");
                while((dir = readdir(d)) != NULL){
                        if(strcmp(dir->d_name,"..")!=0 && strcmp(dir->d_name,".")!=0) // if not . and .. then it is a directory name
                            if(startsWithString(dir->d_name,filterName))
                                printf("\n %s/%s  ", pathName, dir->d_name);
                }

            }
            closedir(d);
            free(pathName);
            free(filterName);

        }
        else if((  (strcmp(argv[1], "list")==0) && (occursPath !=NULL) && (occursFilterP2!=NULL) )
         || (  (strcmp(argv[1], "list")==0) &&  (occursPath2 !=NULL) && (occursFilterP!=NULL) )
         || (  (occursPath0 != NULL) &&  (strcmp(argv[2],"list")==0) && (occursFilterP2!=NULL) )
         || (  (occursPath0 != NULL) &&  (occursFilterP!=NULL) && (strcmp(argv[3],"list")==0))
         || (  (occursFilterP0 != NULL) &&  (strcmp(argv[2],"list")==0) && (occursPath2!=NULL) )
         || (  (occursFilterP0 != NULL) &&  (occursPath!=NULL) && (strcmp(argv[3],"list")==0))

         ){


            char *pathName;
            char *permName;

            if (occursPath0 != NULL){
                if (occursFilterP != NULL)
                {
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                permName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[1]);
                strcpy(permName, argv[2]);
                }
                else //occursFilterN2!=NULL
                {
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                permName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[1]);
                strcpy(permName, argv[3]);
                }
            }
            else if (occursPath != NULL){
                if (occursFilterP0 != NULL)
                {
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                permName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[2]);
                strcpy(permName, argv[1]);
                }
                else //occursFilterN2!=NULL
                {
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                permName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[2]);
                strcpy(permName, argv[3]);
                }
            }
            else { //path is the third argument
                if (occursFilterP0 != NULL)
                {
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                permName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[3]);
                strcpy(permName, argv[1]);
                }
                else //occursFilterN!=NULL
                {
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                permName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[3]);
                strcpy(permName, argv[2]);
                }
            }


            removeSubstr(pathName,path);
            removeSubstr(permName, permission);


            DIR *d;
            d = opendir(pathName);

            if (d == NULL)
            {
               // printf("\n ERROR \n Invalid directory path \n ");
               free(pathName);
               free(permName);
                printf("ERROR \n");
                printf("Invalid directory path d=NULL");
                return(1);
            }
            else
            {


                    char *myTemp = malloc(sizeof(char)*12+1);
                    strcpy(myTemp,"-");
                    strcat(myTemp,permName);

                    char *myTemp2 = malloc(sizeof(char)*12+1);
                    strcpy(myTemp2,"d");
                    strcat(myTemp2,permName);

                printf("SUCCESS");
                struct dirent *dir;

               // printf("\n directory is opened \n ");
                while((dir = readdir(d)) != NULL){

                        if(strcmp(dir->d_name,"..")!=0 && strcmp(dir->d_name,".")!=0) // if not . and .. then it is a directory name
                        {
                            char *temp=malloc(sizeof(char)*100+1);;
                            strcpy(temp,pathName);
                            strcat(temp,"/");
                            strcat(temp,dir->d_name);
                           // printf("temp is : %s \n",temp);
                            //printf("\n permname is: %s \n",permName);
                            char *myPermis =permis(temp);

                             //if(strcmp(permis(temp),permName)==0){
                             if((strcmp(myPermis,myTemp)==0)  || (strcmp(myPermis,myTemp2)==0)){

                               // printf("good");
                                printf("\n %s/%s", pathName, dir->d_name);
                             }

                             free(temp);
                             free(myPermis);

                        }


                }
            free(pathName);
            free(permName);

            free(myTemp);
            free(myTemp2);
            }

            closedir(d);

        }
        else
        {
       // printf("path == 3 input variable problems // \n ERROR \n Invalid directory path \n");
        printf("ERROR \n");
        printf("Invalid directory path");
        }

    }

    //TEMPORAL



    // END OF LIST + PATH + RECURSIVE
    else if(argc==5){ //list path recursive + filtering options
                //if the path string is the 2nd or 3rd or 4th parameter

        char *inputline =malloc(1024*sizeof(char));
        strcpy(inputline,argv[1]);
        strcat(inputline,argv[2]);
        strcat(inputline,argv[3]);
        strcat(inputline,argv[4]);

        occursPath0 = strstr(argv[1],path);
        occursPath = strstr(argv[2],path);
        occursPath2 = strstr(argv[3],path);
        occursPath3 = strstr(argv[4],path);
        occursFilterN0 = strstr(argv[1],filter);
        occursFilterN = strstr(argv[2],filter);
        occursFilterN2 = strstr(argv[3],filter);
        occursFilterN3 = strstr(argv[4],filter);

        occursFilterP0 = strstr(argv[1],permission);
        occursFilterP = strstr(argv[2],permission);
        occursFilterP2 = strstr(argv[3],permission);
        occursFilterP3 = strstr(argv[4],permission);


        occursSectionNr0 = strstr(argv[0],section);
        occursSectionNr = strstr(argv[2],section);
        occursSectionNr2 = strstr(argv[3],section);
        occursSectionNr3 = strstr(argv[4],section);
        occursLineNr0 = strstr(argv[1],line);
        occursLineNr = strstr(argv[2],line);
        occursLineNr2 = strstr(argv[3],line);
        occursLineNr3 = strstr(argv[4],line);


        //list recursive path= name_starts_with=
        if (

            (strstr(inputline,path)!=NULL) && (strstr(inputline,"list")!=NULL) && (strstr(inputline,filter)!=NULL) && (strstr(inputline,"recursive")!=NULL)

              )

            { //path recursive filter list case
           // printf("\n pparam analyisis is good");
            char *pathName;
            char *filterName;
          // remove substrings of path
            if (occursPath0!=NULL){
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[1]);
                //removeSubstr(pathName, path);
                //printf("\n \n path name is: %s \n  \n", pathName);
            }
            else if (occursPath!=NULL){
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[2]);
                //removeSubstr(pathName, path);
            }
            else if(occursPath2!=NULL){ //(occursPath3!=NULL)
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[3]);
               // removeSubstr(pathName, path);
            }
            else if (occursPath3!=NULL){
                pathName = (char*)malloc(strlen(argv[4])*sizeof(char));
                strcpy(pathName, argv[4]);

            }
            else {
                printf("\nERROR\n");
            }
            removeSubstr(pathName, path);


            //remove substring of filter
             if (occursFilterN0!=NULL){
                filterName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(filterName, argv[1]);
               // removeSubstr(filterName, filter);
            }
            else if (occursFilterN!=NULL){
                filterName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(filterName, argv[2]);
                //removeSubstr(filterName, filter);
               // printf("\n \n filter name is: %s \n  \n", filterName);
            }
            else if(occursFilterN2!=NULL){ //(occursFilterN3!=NULL)
                filterName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(filterName, argv[3]);
                //removeSubstr(filterName, filter);

            }
            else if(occursFilterN3 != NULL){
                filterName = (char*)malloc(strlen(argv[4])*sizeof(char));
                strcpy(filterName, argv[4]);
            }
            else{
                printf("\n ERROR \n");
            }

            removeSubstr(filterName, filter);
            //EDDIG OK



            if (startsWithString(pathName, filterName)==1){
                printf("\n starts \n");
            }
            else
                printf("\n doesnt start \n");

            char *temp =malloc(1000*sizeof(char));
            strcpy(temp,pathName);

           filteredRecursiveListing(pathName,1000,filterName,temp);
           //printf("filtered recursive listing, filtername: %s, pathname: %s", filterName, pathName);

           free(pathName);
           free(filterName);


            }

        //PERMISSION LIST RECURSIVE PATH
        else if(  (strstr(inputline,path)!=NULL) && (strstr(inputline,"list")!=NULL) && (strstr(inputline,permission)!=NULL) && (strstr(inputline,"recursive")!=NULL)

        )

        {

            char *pathName;
            char *permName;
          // remove substrings of path
            if (occursPath0!=NULL){
                pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(pathName, argv[1]);
                //removeSubstr(pathName, path);
                //printf("\n \n path name is: %s \n  \n", pathName);
            }
            else if (occursPath!=NULL){
                pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(pathName, argv[2]);
                //removeSubstr(pathName, path);
            }
            else if(occursPath2!=NULL){ //(occursPath3!=NULL)
                pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(pathName, argv[3]);
               // removeSubstr(pathName, path);
            }
            else if (occursPath3!=NULL){
                pathName = (char*)malloc(strlen(argv[4])*sizeof(char));
                strcpy(pathName, argv[4]);

            }
            else {
                printf("\nERROR\n");
            }
            removeSubstr(pathName, path);


            //remove substring of filter
             if (occursFilterP0!=NULL){
                permName = (char*)malloc(strlen(argv[1])*sizeof(char));
                strcpy(permName, argv[1]);
               // removeSubstr(filterName, filter);
            }
            else if (occursFilterP!=NULL){
                permName = (char*)malloc(strlen(argv[2])*sizeof(char));
                strcpy(permName, argv[2]);
                //removeSubstr(filterName, filter);
               // printf("\n \n filter name is: %s \n  \n", filterName);
            }
            else if(occursFilterP2!=NULL){ //(occursFilterN3!=NULL)
                permName = (char*)malloc(strlen(argv[3])*sizeof(char));
                strcpy(permName, argv[3]);
                //removeSubstr(filterName, filter);

            }
            else if(occursFilterP3 != NULL){
                permName = (char*)malloc(strlen(argv[4])*sizeof(char));
                strcpy(permName, argv[4]);
            }
            else{
                printf("\n ERROR \n");
            }

            removeSubstr(permName, permission);
            //EDDIG OK


         char *original = malloc(sizeof(char)*1000);
         strcpy(original,pathName);
         permissionRecursiveListing(pathName,1000,permName,original);
           // free(original);
            free(pathName);
            free(permName);


        }


        //EXTRACT PART

        else if (


           (strstr(inputline,path)!=NULL) && (strstr(inputline,"extract")!=NULL) && (strstr(inputline,line)!=NULL) && (strstr(inputline,section)!=NULL)

             )
        {
                //printf("correct extract input line = %s, section = %s");
                char *pathName;
                char *sectionNumber;
                char *lineNumber;

                //INPUT CASES

                //PATH
                if(occursPath0!=NULL){
                    pathName = (char*)malloc(strlen(argv[1])*sizeof(char));
                    strcpy(pathName, argv[1]);
                    //removeSubstr(pathName, path);
                }
               else if (occursPath!=NULL){
                    pathName = (char*)malloc(strlen(argv[2])*sizeof(char));
                    strcpy(pathName, argv[2]);
                    //removeSubstr(pathName, path);
                //printf("\n \n path name is: %s \n  \n", pathName);
                }
                else if (occursPath2!=NULL){
                    pathName = (char*)malloc(strlen(argv[3])*sizeof(char));
                    strcpy(pathName, argv[3]);
                    //removeSubstr(pathName, path);
                }
                else if (occursPath3!=NULL) { //(occursPath3!=NULL)
                    pathName = (char*)malloc(strlen(argv[4])*sizeof(char));
                    strcpy(pathName, argv[4]);
                    //removeSubstr(pathName, path);
                }
                else{
                    printf("\nERROR\n");
                }
                    removeSubstr(pathName, path);




                //LINE
                if(occursLineNr0!=NULL){

                    lineNumber = (char*)malloc(strlen(argv[1])*sizeof(char));
                    strcpy(lineNumber, argv[1]);
                    //removeSubstr(lineNumber, line);

                }

                else if (occursLineNr!=NULL){
                    lineNumber = (char*)malloc(strlen(argv[2])*sizeof(char));
                    strcpy(lineNumber, argv[2]);
                   // removeSubstr(lineNumber, line);

                }
                else if (occursLineNr2!=NULL){
                    lineNumber = (char*)malloc(strlen(argv[3])*sizeof(char));
                    strcpy(lineNumber, argv[3]);
                   // removeSubstr(lineNumber, line);
                }
                else if (occursLineNr3 !=NULL){
                    lineNumber = (char*)malloc(strlen(argv[4])*sizeof(char));
                    strcpy(lineNumber, argv[4]);
                    //removeSubstr(lineNumber, line);
                }
                else{
                    //removeSubstr(lineNumber, line);
                    printf("\nERROR\n");
                }
                removeSubstr(lineNumber, line);

                //SECTION

                if(occursSectionNr0!=NULL){
                    sectionNumber = (char*)malloc(strlen(argv[1])*sizeof(char));
                    strcpy(sectionNumber, argv[1]);
                }

               else if (occursSectionNr!=NULL){
                    sectionNumber = (char*)malloc(strlen(argv[2])*sizeof(char));
                    strcpy(sectionNumber, argv[2]);
                    //removeSubstr(sectionNumber, section);

                }
                else if (occursSectionNr2!=NULL){
                    sectionNumber = (char*)malloc(strlen(argv[3])*sizeof(char));
                    strcpy(sectionNumber, argv[3]);
                   // removeSubstr(sectionNumber, section);
                }
                else if(occursSectionNr3!=NULL){
                    sectionNumber = (char*)malloc(strlen(argv[4])*sizeof(char));
                    strcpy(sectionNumber, argv[4]);
                   // removeSubstr(sectionNumber, section);
                }
                else{
                    printf("\nERROR\n");
                }
                removeSubstr(sectionNumber, section);


                //printf("path is : %s, sectionr is: %s, line nr is %s ",pathName, sectionNumber, lineNumber);

                //convert sectionNumber, lineNumber to int
                int line_number = atoi(lineNumber);
                int section_number = atoi(sectionNumber);

                extract(pathName, section_number, line_number);

                free(pathName);
                free(lineNumber);
                free(sectionNumber);

        }
        else{
            printf("argc=5 error");
        }

    free(inputline);

    }

    else{
        printf("arg<2=1 // \n ERROR \n Invalid directory path \n ");
    }
    return 0;
}
